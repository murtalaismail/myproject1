/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentsplit1;

/**
 *
 * @author user
 */
public class SentSplit1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // This program accepts sentence from the and output the first character of each word.
        java.util.Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter a sentence");
        String sentence = sc.nextLine();
        String words[] = sentence.split(" ");
        for(String word:words)
        {
            System.out.print(word.charAt(0)+" ");
        }
    }
}
